FROM java:8
MAINTAINER kartzan
COPY target/olympic-0.0.1-SNAPSHOT.jar olympic.jar
RUN bash -c "touch /olympic.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","/olympic.jar"]