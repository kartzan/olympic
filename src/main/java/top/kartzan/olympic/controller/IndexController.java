package top.kartzan.olympic.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import top.kartzan.olympic.dto.MedalDto;
import top.kartzan.olympic.entity.Medal;
import top.kartzan.olympic.entity.Schedule;
import top.kartzan.olympic.service.MedalService;
import top.kartzan.olympic.service.ScheduleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@SessionAttributes(value = { "venuename", "itemcodename", "startDate" })
public class IndexController {

    @Autowired
    private MedalService medalService;

    @Autowired
    private ScheduleService scheduleService;


    @GetMapping("/world")
    public ModelAndView world() {
        ModelAndView modelAndView = new ModelAndView();
        List<Medal> list = this.medalService.list();
        List<MedalDto> result = list.stream().map(MedalDto::valueOf).collect(Collectors.toList());
        modelAndView.addObject("list", result);
        modelAndView.addObject("index", "world");
        modelAndView.setViewName("world");
        return modelAndView;
    }

    @GetMapping({"/", "/totalRank"})
    public ModelAndView totalRank() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("list", this.medalService.list());
        modelAndView.addObject("index", "totalRank");
        modelAndView.setViewName("totalRank");
        return modelAndView;
    }

    @GetMapping("/schedule")
    public ModelAndView schedule(@RequestParam(value = "venuename", required = false) String venuename,
                                 @RequestParam(value = "itemcodename", required = false) String itemcodename,
                                 @RequestParam(value = "startDate", required = false) String startDate,
                                 @RequestParam(value = "page", required = false)Integer page,
                                 @RequestParam(value = "pageNo", required = false)Integer pageNo) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();
        if (!Objects.equals(startDate, session.getAttribute("startDate"))
            || !Objects.equals(venuename, session.getAttribute("venuename"))
                || !Objects.equals(itemcodename, session.getAttribute("itemcodename"))) {
            page = 1;
        }
        List<String> itemList = this.scheduleService.getItemList();
        List<String> venueList = this.scheduleService.getVenueList();
        if (page == null || pageNo == null) {
            page = 1;
            pageNo = 20;
        }
        PageHelper.startPage(page, pageNo);
        List<Schedule> scheduleList = this.scheduleService.searchSchedule(venuename, itemcodename, startDate);
        PageInfo<Schedule> pageInfo = new PageInfo<>(scheduleList);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("itemList", itemList);
        modelAndView.addObject("venueList", venueList);
        modelAndView.addObject("scheduleList", pageInfo);
        modelAndView.addObject("page", page);
        modelAndView.addObject("pageNo", pageNo);
        modelAndView.addObject("venuename", venuename);
        modelAndView.addObject("itemcodename", itemcodename);
        modelAndView.addObject("startDate", startDate);
        modelAndView.addObject("index", "schedule");
        modelAndView.setViewName("dailyMatch");
        return modelAndView;
    }

    @RequestMapping("/knowMore")
    public ModelAndView knowMore() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("index", "knowMore");
        return modelAndView;
    }
}
