package top.kartzan.olympic.dao;

import org.apache.ibatis.annotations.Mapper;
import top.kartzan.olympic.entity.Medal;

import java.util.List;

@Mapper
public interface MedalDao {

    List<Medal> listMedal();
}

