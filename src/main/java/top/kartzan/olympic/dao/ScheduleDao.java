package top.kartzan.olympic.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.kartzan.olympic.entity.Schedule;

import java.util.List;

@Mapper
public interface ScheduleDao  {

    List<String> getItemList();

    List<String> getVenueList();

    List<Schedule> searchSchedule(@Param("venuename")String venuename, @Param("itemcodename")String itemcodename, @Param("startDate")String startDate);

}

