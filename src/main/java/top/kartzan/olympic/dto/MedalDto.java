package top.kartzan.olympic.dto;

import lombok.Data;
import org.springframework.beans.BeanUtils;
import top.kartzan.olympic.entity.Medal;

@Data
public class MedalDto {

    private Integer bronze;

    private Integer rank;

    private Integer count;

    private Integer silver;

    private String name;

    private Integer gold;

    private String countryid;

    private Integer value;

    public static final MedalDto valueOf(Medal medal) {
        MedalDto medalDto = new MedalDto();
        BeanUtils.copyProperties(medal, medalDto);
        medalDto.value = medalDto.getCount();
        return medalDto;
    }
}
