package top.kartzan.olympic.entity;


import lombok.Data;

@Data
public class Medal {

    private Integer bronze;

    private Integer rank;

    private Integer count;

    private Integer silver;

    private String name;

    private Integer gold;

    private String countryid;


}

