package top.kartzan.olympic.entity;


import lombok.Data;

@Data
public class Schedule {
    
    private String statusname;
    
    private String subitemname;
    
    private String enddatecn;
    
    private String title;
    
    private String venuename;
    
    private String status;
    
    private String homename;
    
    private String awayname;
    
    private String itemcodename;
    
    private Integer id;
    
    private String homeid;
    
    private String h5pageid;
    
    private String itemcode;
    
    private String vrtotalurl;
    
    private String albumurl;
    
    private String vrlivecode;
    
    private String deletedflag;
    
    private String documentcode;
    
    private String totaltitle;
    
    private String imageurl;
    
    private String vrliveurl;
    
    private String pageid;
    
    private String startdatecn;
    
    private String subitemcode;
    
    private String awayid;
    
    private String totalguid;
    
    private String combatflag;
    
    private String lockflag;
    
    private String mvlivecode;
    
    private String liveurl;
    
    private String reserve3;
    
    private String venue;
    
    private String awayscore;
    
    private String reserve2;
    
    private String homecode;
    
    private String reserve1;
    
    private String vrtotalcode;
    
    private String homescore;
    
    private String livecode;
    
    private String totalurl;
    
    private String mvliveurl;
    
    private String awaycode;
    
    private String adcode;
    
    private String medal;

}

