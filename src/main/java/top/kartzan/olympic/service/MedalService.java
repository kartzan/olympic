package top.kartzan.olympic.service;

import top.kartzan.olympic.entity.Medal;

import java.util.List;

public interface MedalService {

    List<Medal> list();
}

