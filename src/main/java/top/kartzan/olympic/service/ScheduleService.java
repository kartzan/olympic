package top.kartzan.olympic.service;

import top.kartzan.olympic.entity.Schedule;

import java.util.List;

public interface ScheduleService {

    List<String> getItemList();

    List<String> getVenueList();

    List<Schedule> searchSchedule(String venuename, String itemcodename, String startDate);

}

