package top.kartzan.olympic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.kartzan.olympic.dao.MedalDao;
import top.kartzan.olympic.entity.Medal;
import top.kartzan.olympic.service.MedalService;

import java.util.List;

@Service("medalService")
public class MedalServiceImpl implements MedalService {

    @Autowired
    private MedalDao baseMapper;

    @Override
    public List<Medal> list() {
        return baseMapper.listMedal();
    }

}

