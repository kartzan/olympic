package top.kartzan.olympic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.kartzan.olympic.dao.ScheduleDao;
import top.kartzan.olympic.entity.Schedule;
import top.kartzan.olympic.service.ScheduleService;

import java.util.List;

@Service("scheduleService")
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleDao baseMapper;

    @Override
    public List<String> getItemList() {
        return baseMapper.getItemList();
    }

    @Override
    public List<String> getVenueList() {
        return baseMapper.getVenueList();
    }

    @Override
    public List<Schedule> searchSchedule(String venuename, String itemcodename, String enddatecn) {
        return baseMapper.searchSchedule(venuename, itemcodename, enddatecn);
    }
}

